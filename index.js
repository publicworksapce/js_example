// salam sara & vahid
//.....................variable........................

const productsData = [{
    id: 1, title: "queen panel bed", price: 10.99, imageUrl: "images/product-1.jpeg", quantity: 1,
}, {
    id: 2, title: "saheb panel bed", price: 14.99, imageUrl: "images/product-2.jpeg ", quantity: 1,
}, {
    id: 3, title: "folan panel bed", price: 7.99, imageUrl: "images/product-3.jpeg", quantity: 1,
}, {
    id: 4, title: "twin panel bed", price: 11.99, imageUrl: "images/product-4.jpeg", quantity: 1,
}, {
    id: 5, title: "fridge", price: 4.99, imageUrl: "images/product-5.jpeg", quantity: 1,
}, {
    id: 6, title: "fridge", price: 23.99, imageUrl: "images/product-6.jpeg", quantity: 1,
}, {
    id: 7, title: "dresser", price: 42.99, imageUrl: "images/product-7.jpeg", quantity: 1,
}, {
    id: 8, title: "couch", price: 24.99, imageUrl: "images/product-8.jpeg", quantity: 1,
},];
const products = document.querySelector(".productContainer");

const allBtn = document.querySelectorAll('.add-to-cart')
const numberOfBasket = document.querySelector("#numberOfBasket")
const removeBtn = document.querySelector(".removeBtn")
const confirmBtn = document.querySelector(".confirmBtn")
const basketIcon = document.querySelector(".cardContainer")
// add event listener
productFunction();
setResponseToButtons()
let basket = [];

// functions
function productFunction() {
    productsData.forEach((item) => {
        const productMake = document.createElement("div");
        productMake.innerHTML = `<div class="product">
      <div class="imgProduct">
          <img src=${item.imageUrl} alt="oops!">
      </div>
      <div class="priceTitle">
          <h5 class="title">${item.title} </h5>
          <p class="price">${item.price} </p>
      </div>
      <div class="buttons">
          <button value="Add to Cart" class="btn add-to-cart" id=${item.id}>Add to Cart</button>
      </div>
  </div>`;
        products.appendChild(productMake);
    });
}

function setResponseToButtons() {
    const allBtn = document.querySelectorAll('.add-to-cart')
    allBtn.forEach((btn) => {
        btn.addEventListener('click', (e) => {
            const product = productsData.find((item) => item.id == e.target.id)
            const basketItem = basket.find((item) => item.id == e.target.id)
            if (!basketItem) {
                //adding the first product to the basket
                basket = [...basket, {...product, quantity: 1}]
                e.target.innerText = 'in cart'
            } else {
                // check if its in basket or not
                basket = basket.filter((item) => item.id != e.target.id)
                e.target.innerText = 'Add to Cart'
            }
            numberOfBasket.innerText = basket.length
            modalsItem()
        })
    })
}

function modalsItem() {
    const modal = document.querySelector(".modal")
    removeAllChildren(modal)
    basket.forEach(item => {
        const selectedItemsInnerHtml = `<div class="modalCenter">
      
      <div class="modalContainer">
         <div class="selectedProduct">
           <div class="modalImageProduct">
               <img src=${item.imageUrl} alt="oops!">
           </div>
           <div class="modalPriceTitle">
               <h5 class="modalTitle">${item.title} :</h5>
               <p class="modalPrice">${item.price}</p>
           </div>
           <div>
               <div class="cart-item-conteoller" >
                   <i class="fas fa-chevron-up btns" id=${item.id}></i>
                   <p class="quantity">${item.quantity}</p>
                   <i class="fas fa-chevron-down btns" id=${item.id}></i>
                 </div>
           </div>
       </div>
       
       </div>
      </div>`
        const selectedItems = document.createElement("div");
        selectedItems.innerHTML = selectedItemsInnerHtml
        modal.appendChild(selectedItems)
        //console.log(modal)
    })
    changingQuantity()
    calculateTotalCost()
}

function removeAllChildren(modal) {
    while (modal.firstChild) {
        modal.removeChild(modal.firstChild)
    }
}

function changingQuantity() {
    const eachQuantity = document.querySelectorAll(".btns")

    eachQuantity.forEach(btn => {
        btn.addEventListener("click", (e) => {
            const btnClassList = e.target.className
            const productClicked = basket.find((p) => p.id == e.target.id)
            //console.log(productClicked)
            if (btnClassList.includes("fa-chevron-up")) {
                productClicked.quantity = Number(productClicked.quantity) + 1
                modalsItem()
                //calculateTotalCost()
            } else {
                productClicked.quantity = Number(productClicked.quantity) - 1
                modalsItem()

                if (Number(productClicked.quantity) <= 0) {
                    productClicked.quantity = 1
                    modalsItem()
                }
            }

        })
    })
}

function calculateTotalCost() {
    let totalPrice = document.querySelector(".totalPriceTag")
    //changingQuantity()
    const total = basket.reduce((acc, cur) => {
        return cur.quantity * cur.price + acc
    }, 0)
    totalPrice.innerHTML = total.toFixed(2)
}

removeBtn.addEventListener("click", () => {
    basket = [];
    modalsItem();
    resetProductComponent();
})

confirmBtn.addEventListener("click", () => {
    const modalContainer = document.querySelector(".modalContainer")
    modalContainer.style.display = "none"
})

basketIcon.addEventListener("click", () => {
    const modalContainer = document.querySelector(".modalContainer")
    modalContainer.style.display = "block"
})

function resetProductComponent() {
    const btnValue = document.querySelectorAll(".add-to-cart");
    btnValue.forEach((btn) => btn.innerHTML = "Add to Cart")
}